# Changelog


## Unreleased

### Features

* Add stochasticity argument. [Stavros Korokithakis]

* Add pre-commit hook. [Stavros Korokithakis]

### Fixes

* Fix cli interface. [Stavros Korokithakis]

* Actually fix types. [Stavros Korokithakis]

* Fix types. [Stavros Korokithakis]

* Fix bug where the first month was skipped. [Stavros Korokithakis]

* Make project name optional. [Stavros Korokithakis]


