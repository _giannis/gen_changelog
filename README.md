gen_changelog
=============

gen_changelog is a very simple script that generates a changelog from a repository with
commit messages compatible with [Conventional
Commits](https://www.conventionalcommits.org/). The main difference of this script from
[gitchangelog](https://github.com/sarnold/gitchangelog) is that this script will
generate sections from dates/months instead of git tags of versions.


Usage
-----

Just run it in the root of your repository:

```bash
$ ./gen_changelog "Your project's name"
```

You're done.


With pre-commit
---------------

To use this with pre-commit, add this to your `.pre-commit-config.yaml`:

```yaml
- repo: https://gitlab.com/stavros/gen_changelog.git
  rev: ccc6fa1dbe1a937c9b729e356b5fd91bf9d59ca4
  hooks:
  - id: gen-changelog
    stages: [commit]
```
